<?php
$container = require_once __DIR__ . '/di.php';
/** @var \Doctrine\ORM\EntityManager $entityManager */
$entityManager = $container['entity_manager'];

// Put your sample code here!
echo "Let's rock.\n";

/** @var \Infrastructure\Repository\DoctrinePatientsRepository $repository */
$repository = $entityManager->getRepository('Derp\Entity\Patient');


$personalInformation = \Derp\ValueObject\PersonalInformation::fromDetails(
    \Derp\ValueObject\FullName::fromParts('Toon', 'Verwerft'),
    \Derp\ValueObject\BirthDate::fromYearMonthDayFormat('1987-10-08'),
    new \Derp\ValueObject\Sex('male')
);

$patientId = \Derp\Id\PatientId::fromString(\Rhumsaa\Uuid\Uuid::uuid4()->toString());
$patient = \Derp\Entity\Patient::walkIn($patientId, $personalInformation, 'indication');
$repository->add($patient);
$entityManager->flush();


$iterator = $repository->findByLastName('Verwerft');
$iterator = $repository->findByBirthDate(\Derp\ValueObject\BirthDate::fromYearMonthDayFormat('1987-10-08'));
var_dump(iterator_to_array($iterator));


