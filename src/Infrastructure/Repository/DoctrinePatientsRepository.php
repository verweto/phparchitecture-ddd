<?php

namespace Infrastructure\Repository;

use Derp\Entity\Patient;
use Derp\Repository\PatientsRepository;
use Derp\ValueObject\BirthDate;
use Doctrine\ORM\EntityRepository;

/**
 * Class DoctrinePatientsRepository
 *
 * @package Derp\Infrastructure
 */
class DoctrinePatientsRepository extends EntityRepository
    implements PatientsRepository
{
    /**
     * @param Patient $patient
     */
    public function add(Patient $patient)
    {
        $this->getEntityManager()->persist($patient);
    }

    /**
     * @param Patient $patient
     */
    public function remove(Patient $patient)
    {
        $this->getEntityManager()->remove($patient);
    }

    /**
     * @param $lastName
     *
     * @return Patient[]
     */
    public function findByLastName($lastName)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('e')
            ->from('Derp\Entity\Patient', 'e')
            ->where('e.personalInformation.name.lastName = :lastName');

        return $qb->getQuery()->iterate(['lastName' => $lastName]);
    }

    /**
     * @param $birthDate
     *
     * @return Patient[]
     */
    public function findByBirthDate(BirthDate $birthDate)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('e')
            ->from('Derp\Entity\Patient', 'e')
            ->where('e.personalInformation.dateOfBirth.date = :dateOfBirth');

        return $qb->getQuery()->iterate([
            'dateOfBirth' => $birthDate->toDateTime()->format('Y-m-d 00:00:00')
        ]);
    }
}
