<?php


namespace Derp\Entity\Exception;

use DomainException;

/**
 * Class PatientAllreadyArrivedException
 *
 * @package Derp\Entity\Exception
 */
class PatientAllreadyArrivedException extends DomainException
{
}