<?php
namespace Derp\ValueObject;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class FullName
{
    /**
     * @ORM\Column(type="string")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     */
    private $lastName;

    private function __construct()
    {
    }

    public static function fromParts($firstName, $lastName)
    {
        $patient = new static();
        $patient->firstName = $firstName;
        $patient->lastName = $lastName;

        return $patient;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }
}