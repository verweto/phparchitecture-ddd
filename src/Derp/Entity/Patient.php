<?php

namespace Derp\Entity;

use Assert;
use Derp\Entity\Exception\PatientAllreadyArrivedException;
use Derp\Id\PatientId;
use Derp\ValueObject\PersonalInformation;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Patient
 *
 * @package Derp\Entity
 * @ORM\Entity(repositoryClass="Infrastructure\Repository\DoctrinePatientsRepository")
 */
class Patient
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="patient_id")
     * @var PatientId
     */
    private $patientId;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $indication;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $arrived;

    /**
     * @var PersonalInformation
     * @ORM\Embedded(class="Derp\ValueObject\PersonalInformation", columnPrefix=false)
     */
    private $personalInformation;

    /**
     * @param PersonalInformation $personalInformation
     * @param                     $indication
     * @param                     $arrived
     */
    private function __construct(
        PatientId $patientId,
        PersonalInformation $personalInformation,
        $indication,
        $arrived
    )
    {
        $this->patientId = $patientId;
        $this->personalInformation = $personalInformation;
        $this->indication = $indication;
        $this->arrived = $arrived;
    }

    /**
     * @param PatientId           $patientId
     * @param PersonalInformation $personalInformation
     * @param                     $indication
     *
     * @return Patient
     */
    public static function announce(PatientId $patientId, PersonalInformation $personalInformation, $indication)
    {
        return new Patient($patientId, $personalInformation, $indication, false);
    }

    /**
     * @param PatientId           $patientId
     * @param PersonalInformation $personalInformation
     * @param                     $indication
     *
     * @return Patient
     */
    public static function walkIn(PatientId $patientId, PersonalInformation $personalInformation, $indication)
    {
        return new Patient($patientId, $personalInformation, $indication, true);
    }

    /**
     * @return string
     */
    public function getIndication()
    {
        return $this->indication;
    }

    /**
     * @param $indication
     */
    public function changeIndication($indication)
    {
        Assert\that($indication)
            ->string()
            ->betweenLength(1, 1000);

        $this->indication = $indication;
    }

    /**
     * @return bool
     */
    public function hasArrived()
    {
        return $this->arrived;
    }

    /**
     *
     */
    public function registerArrival()
    {
        if ($this->hasArrived()) {
            throw new PatientAllreadyArrivedException();
        }

        $this->arrived = true;
    }
}