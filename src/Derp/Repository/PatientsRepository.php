<?php


namespace Derp\Repository;
use Derp\Entity\Patient;
use Derp\ValueObject\BirthDate;

/**
 * Class PatientsRepository
 *
 * @package Derp\Repository
 */
interface PatientsRepository
{

    /**
     * @param Patient $patient
     */
    public function add(Patient $patient);

    /**
     * @param Patient $patient
     */
    public function remove(Patient $patient);

    /**
     * @param $lastName
     *
     * @return Patient[]
     */
    public function findByLastName($lastName);

    /**
     * @param BirthDate $birthDate
     *
     * @return Patient[]
     */
    public function findByBirthDate(BirthDate $birthDate);

}